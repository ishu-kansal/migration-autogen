#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <cstdio>
#include <ctime>
#include <unistd.h>

using namespace std;

/*
REQUIRES: string to search, char to search for, int number in string
RETURNS: index of Nth occurence of char in string
*/
int findNthOccur(string str, char ch, int N);

/*
REQUIRES: filename of input file
RETURNS: make and model combos in standard migration format
*/
string generateMakeModelCombosFile(string filename, string type);

/*
REQUIRES: string for blade and turbine combos
 created in same format as generateMakeModelCombosFile
RETURNS: printable string that formats entire migration file for make/model combinations
*/
string generateOutputFileStrCombos(string turbineCombos, string bladeCombos);

/*
REQUIRES: filename of input file, type of field {make, model}
RETURNS: vector of unique types
*/
vector<string> getUniqueTypes(string filename, string type);

/*
REQUIRES: string vectors of blade makes, blade models, turbine makes, turbine models
RETURNS: printable string that formats migration file for unique makes and models for assets
*/
string generateOutputFileStrUniques(vector<string> bladeMakes, vector<string> bladeModels,
	vector<string> turbineMakes, vector<string> turbineModels );

/*
RETURNS: current time in YYYYMthMthDDHHMMSS format
*/
string generateTime();

/*
REQUIRES: seconds to convert microseconds to
RETURNS: number of microseconds in 'seconds' 
*/
int getMicroS(int seconds);



int main()
{
	//generate combinations migration output file name
	string outFileNameCombos = generateTime() + "_update_field_combinations_table.ts";
	
	// File pointer
	fstream fin;
	
	//read in turbine and blade input data from csv spreadsheet
	cout << "Enter file Name for Turbine (csv): ";
	string fileNameTurbine;
	cin >> fileNameTurbine;
	fin.open(fileNameTurbine);
	if(!fin.is_open()){
		cout << "Turbine file not found" << endl;
		return -1;
	}

	cout << "Enter file Name for Blade (csv): ";
	string fileNameBlade;
	cin >> fileNameBlade;
	fin.open(fileNameBlade);
	if(!fin.is_open()){
		cout << "Blade file not found" << endl;
		return -2;
	}
	
	//generate format for turbine and blade combos
	string turbineCombos = generateMakeModelCombosFile(fileNameTurbine, "turbine");
	string bladeCombos = generateMakeModelCombosFile(fileNameBlade, "blade");
	
	ofstream combosOutFile(outFileNameCombos);
	//generate format for combos migration file and print to migrations file
	string output = generateOutputFileStrCombos(turbineCombos, bladeCombos);
	combosOutFile << output << endl;

	//sleep to create time differential between two migrations files
	usleep(getMicroS(3));

	//generate output file name for unique makes/models migrations file
	string outFileNameUniques = generateTime() + "_update_standared_make_model_fields.ts";

	//calculate all unique fields
	vector<string> bladeMakesList = getUniqueTypes(fileNameBlade, "make");
	vector<string> bladeModelsList = getUniqueTypes(fileNameBlade, "model");

	vector<string> turbineMakesList = getUniqueTypes(fileNameTurbine, "make");
	vector<string> turbineModelsList = getUniqueTypes(fileNameTurbine, "model");

	ofstream uniqueOutFile(outFileNameUniques);

	//print out unique fields in migrations format
	string output2 = generateOutputFileStrUniques(bladeMakesList, bladeModelsList, turbineMakesList, turbineModelsList);
	uniqueOutFile << output2;

	//let user know that program is completed
	cout << "Success! Please copy files to migrations folder" << endl;
}


int getMicroS(int seconds){
	return seconds * 1000000;
}

string generateTime(){
	std::time_t rawtime;
    std::tm* timeinfo;
    char buffer [80];

    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);
    std::strftime(buffer,80,"%Y%m%d%H%M%S",timeinfo);

	string myTime = buffer;
	return myTime;
}

int findNthOccur(string str, char ch, int N)
{
    int occur = 0;
 
    // Loop to find the Nth
    // occurrence of the character
    for (int i = 0; i < str.length(); i++) {
        if (str[i] == ch) {
            occur += 1;
        }
        if (occur == N)
            return i;
    }
    return -1;
}

string generateMakeModelCombosFile(string filename, string type)
{
	fstream fin(filename);
	vector<string> comboStrings;
	string line;
	//take data and format into knex migration form
	getline(fin, line);
	while (getline(fin, line)){
		int nextIndexOfComma = findNthOccur(line, ',', 1);
		int indexOfComma = line.find(',');
		string columns = line.substr(indexOfComma+1, nextIndexOfComma);
		int thirdIndex = columns.find(',');
		string make = columns.substr(0, thirdIndex);
		int length = columns.length();
		string model = columns.substr(thirdIndex+1, length - thirdIndex);
		string combo;
		if(type == "turbine"){
			combo = "(_turbineMakeId," + string(" _turbineModelId, \'") + make +
				"\',\'" + model + "\')";
		}
		if(type == "blade"){
			combo = "(_bladeMakeId," + string(" _bladeModelId, \'") + make +
				"\',\'" + model + "\')";
		}
		
		comboStrings.push_back(combo);
	}

	string result;
	for (int i = 0; i < comboStrings.size()-1; i++){
		result += "        " + comboStrings[i];
		result += ",\n";
	}
	result += "        " + comboStrings[comboStrings.size()-1];
	result+= ";\n";

	return result;
}

vector<string> getUniqueTypes(string filename, string type)
{
	fstream fin(filename);
	vector<string> makes;
	vector<string> models;
	string line;
	//take data and push into vector
	getline(fin, line);
	while (getline(fin, line)){
		int nextIndexOfComma = findNthOccur(line, ',', 1);
		int indexOfComma = line.find(',');
		string columns = line.substr(indexOfComma+1, nextIndexOfComma);
		int thirdIndex = columns.find(',');
		string make = columns.substr(0, thirdIndex);
		int length = columns.length();
		string model = columns.substr(thirdIndex+1, length - thirdIndex);
		
		if(type == "make"){
			makes.push_back(make);
		}
		if(type == "model"){
			models.push_back(model);
		}
	}

	//filter out unique elements in vector
	if(type == "make"){
		makes.erase(unique(makes.begin(), makes.end()), makes.end());
		makes.push_back("Other");
		return makes;
	}
	models.erase(unique(models.begin(), models.end()), models.end());
	models.push_back("Other");
	return models;

}

string generateOutputFileStrCombos(string turbineCombos, string bladeCombos){
	string output;
	output += "import { Knex } from \"knex\";\n";
	output += "\nexport async function up(knex: Knex): Promise<void> {\n";
	output += "  return knex.raw(`\n";
	output += "    DO $$\n    DECLARE\n      _bladeTypeId INT;\n      _turbineTypeId INT;\n      _bladeMakeId INT;\n      _bladeModelId INT;\n      _turbineMakeId INT;\n      _turbineModelId INT;\n\n";
	output += "    BEGIN\n";
	output += "      _bladeTypeId = (SELECT db_id FROM asset.types WHERE internal_name = 'blade' AND global_organization_id is null AND is_fixed AND deleted is null);\n";
	output += "      _turbineTypeId = (SELECT db_id FROM asset.types WHERE internal_name = 'turbine' AND global_organization_id is null AND is_fixed AND deleted is null);\n";
	output += "      _bladeMakeId = (SELECT db_id from asset.field_definitions where type_id = _bladeTypeId and internal_name = 'blade_make' and global_organization_id is null and is_fixed);\n";
	output += "      _bladeModelId = (SELECT db_id from asset.field_definitions where type_id = _bladeTypeId and internal_name = 'blade_model' and global_organization_id is null and is_fixed);\n";
	output += "      _turbineMakeId = (SELECT db_id from asset.field_definitions where type_id = _turbineTypeId and internal_name = 'turbine_make' and global_organization_id is null and is_fixed);\n";
	output += "      _turbineModelId = (SELECT db_id from asset.field_definitions where type_id = _turbineTypeId and internal_name = 'turbine_model' and global_organization_id is null and is_fixed);\n";
	output += "\n    delete from asset.field_combinations;\n";
	output += "\n    -- Turbines\n";
	output += "    insert into asset.field_combinations\n";
	output += "      (independent_field_id, dependent_field_id, independent_field_value, dependent_field_value)\n";
	output += "      VALUES\n";
	output += turbineCombos + "\n";
	output += "    -- Blades\n";
	output += "    insert into asset.field_combinations\n";
	output += "      (independent_field_id, dependent_field_id, independent_field_value, dependent_field_value)\n";
	output += "      VALUES\n";
	output += bladeCombos + "\n";
	output += "    END $$;\n";
	output += "`);\n}\n";
	output += "\nexport async function down(knex: Knex): Promise<void> {\n";
	output += "   return Promise.resolve();\n";
	output += "}";
	return output;
}

string generateOutputFileStrUniques(vector<string> bladeMakes, vector<string> bladeModels,
	vector<string> turbineMakes, vector<string> turbineModels )
{
	string output;
	output += "import { Knex } from \"knex\";\n";
	output += "\nexport async function up(knex: Knex): Promise<void> {\n";
	output += "  return knex.raw(`\n";
	output += "    DO $$\n    DECLARE\n      _bladeTypeId INT;\n      _turbineTypeId INT;\n";
	output += "\n    BEGIN\n";
	output += "      _bladeTypeId = (SELECT db_id FROM asset.types WHERE internal_name = 'blade' AND global_organization_id is null AND is_fixed AND deleted is null);\n";
    output += "      _turbineTypeId = (SELECT db_id FROM asset.types WHERE internal_name = 'turbine' AND global_organization_id is null AND is_fixed AND deleted is null);\n";

	output += "\n      -- Blade Make\n";
	output += "      update asset.field_definitions\n";
	output += "      set field_options = '{";
	for(int i = 0; i < bladeMakes.size(); i++){
		output += bladeMakes[i];
		if(i != bladeMakes.size()-1)
			output += ",";
	}
	output += "}'\n";

	output += "      where is_fixed and global_organization_id is null\n";
	output += "        and type_id = _bladeTypeId\n";
	output += "          and internal_name = 'blade_make';\n";


	output += "\n      -- Blade Model\n";
	output += "      update asset.field_definitions\n";
	output += "      set field_options = '{";
	for(int i = 0; i < bladeModels.size(); i++){
		output += bladeModels[i];
		if(i != bladeModels.size()-1)
			output += ",";
	}
	output += "}'\n";

	output += "      where is_fixed and global_organization_id is null\n";
	output += "        and type_id = _bladeTypeId\n";
	output += "          and internal_name = 'blade_model';\n";


	output += "\n      -- Turbine Make\n";
	output += "      update asset.field_definitions\n";
	output += "      set field_options = '{";
	for(int i = 0; i < turbineMakes.size(); i++){
		output += turbineMakes[i];
		if(i != turbineMakes.size()-1)
			output += ",";
	}
	output += "}'\n";

	output += "      where is_fixed and global_organization_id is null\n";
	output += "        and type_id = _turbineTypeId\n";
	output += "          and internal_name = 'turbine_make';\n";



	output += "\n      -- Turbine Model\n";
	output += "      update asset.field_definitions\n";
	output += "      set field_options = '{";
	for(int i = 0; i < turbineModels.size(); i++){
		output += turbineModels[i];
		if(i != turbineModels.size()-1)
			output += ",";
	}
	output += "}'\n";

	output += "      where is_fixed and global_organization_id is null\n";
	output += "        and type_id = _turbineTypeId\n";
	output += "          and internal_name = 'turbine_model';\n";

	output += "\n    END $$;\n";
	output += "  `);\n}\n";

	output += "\nexport async function down(knex: Knex): Promise<void> {\n";
	output += "   return Promise.resolve();\n";
	output += "}";

	return output;
}


