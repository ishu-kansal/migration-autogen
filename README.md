## Name
Migration-Autogen

## Description
This is a project to generate migrations files for updating wind make and model combinations for database migrations

## Installation
No installation necessary. Simply run the program by going to terminal in this folder and typing in the following command:
`./gen.out`

## Usage
Download the make and models combinations spreadsheets for blade and turbine as csv.

Rename the files something convenient like turbines.csv and blades.csv

Run the program by going to terminal in repo directory and `./gen.out`

You will be prompted to enter the file names. 

Enter turbines.csv (or whatever you named the turbines file), and similar for blades

The migrations files will be generated automatically. Simply copy the files from the repo folder to the database migrations folder and run knex!

## Authors and Acknowledgement
Author: Ishu Kansal
